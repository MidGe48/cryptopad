/*    Copyright (C) 2018  Michel de Greef

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// main
package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"zunob.com/cryptopad"
)

func init() {

}

func main() {

	fmt.Println("Starting cryptopad...\n")

	// setup values from flgs or use defaults

	infptr := flag.String("i", "file.in", "file path to read from")
	outfptr := flag.String("o", "file.out", "file path to write to")
	mode := flag.Int("m", 1, "1 for encrypt, -1 for decrypt")

	keyfptr := flag.String("k", "key", "file path to encrypt/decrypt with")
	pin := flag.Int("p", 0, "optional pin number")
	bufsz := flag.Int("s", 4096, "desired memory buffer size")
	help := flag.Bool("h", false, "this message!")
	flag.Parse()

	// help
	if *help {
		fmt.Println("Usage:  main  [OPTION]...\n\n" +
			"Encrypt or decrypt a given file based on a given key\n\n" +
			"-i=[INPUT_FILE]    the path/name of the file to be encrypted or decrypted, the default value is 'file.in'\n" +
			"-o=[OUPUT_FILE]    the path/name of the output file; the default value is 'file.out'\n" +
			"-k=[KEY_FILE]      the path/name of the key file; the default value is 'key'\n" +
			"-p=int             optional pin number to be used in concert with a key file for encryption and decryption" +
			"-s=[SIZE]          the memory size in _ := bytes to be used during the encryt/decrypt process: the default value is 4096 bytes\n" +
			"-m=[1|-1]          specifies whether to do an encryption(1) or a decryption(-1); the default value is encrypt(1)\n" +
			"-h                 display this help message")

	}
	// Input file
	inf, err := os.Open(*infptr)
	if err != nil {
		log.Fatal(fmt.Errorf("Unable to open %v because -->  %v", *infptr, err))
	}
	defer func() {
		if err = inf.Close(); err != nil {
			log.Fatal(fmt.Errorf("Unable to close %v because --> %v", *infptr, err))
		}
	}()

	// Key file
	keyf, err := os.Open(*keyfptr)
	if err != nil {
		log.Fatal(fmt.Errorf("Unable to open %v because -->  %v", *keyfptr, err))
	}
	defer func() {
		if err = keyf.Close(); err != nil {
			log.Fatal(fmt.Errorf("Unable to close %v because --> %v", *keyfptr, err))
		}
	}()

	// Output file
	// First check if file exists already, error if it does!
	if _, err := os.Stat(*outfptr); !os.IsNotExist(err) {
		// path exists
		log.Fatalf("Error: the output file \"%v\" exists already!", *outfptr)
	}

	outf, err := os.Create(*outfptr)
	if err != nil {
		log.Fatal(fmt.Errorf("Unable to create %v because --> %v", *outfptr, err))
	}
	defer func() {
		if err = outf.Sync(); err != nil {
			log.Fatal(fmt.Errorf("Unable to sync %v because --> %v", *outfptr, err))
		}
		if err = outf.Close(); err != nil {
			log.Fatal(fmt.Errorf("Unable to close %v because --> %v", *outfptr, err))
		}
	}()

	// check that size of key file > 150% of input file
	// to cater for the exclusion of repeating bytes values
	// and pin size
	ifs, err := inf.Stat()
	if err != nil {
		log.Fatal(fmt.Errorf("Unable to get os stats for %v because --> %v", *infptr, err))
	}

	kfs, err := keyf.Stat()
	if err != nil {
		log.Fatal(fmt.Errorf("Unable to get os stats for %v because --> %v", *keyfptr, err))
	}

	if ifs.Size() > (kfs.Size()-int64(*pin))*66/100 {
		log.Fatal(fmt.Errorf("key file size less the pin should be at least 150% of message file size"))
	}

	// invoke the encryption with the given parameters
	err = cryptopad.FileCrypt(inf, keyf, outf, bufsz, pin, mode)
	if err != nil {
		log.Fatal(fmt.Errorf("Cannot run FileCrypt because --> %v", err))
	}

}
