/*    Copyright (C) 2018  Michel de Greef

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Package cryptopad provides an OTP encryption facility
// it accepts any stream as a binary key
// cryptopad.go
package cryptopad

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

const (
	// mode is equivalent to:|| err=io.EOF
	//             adding for encryption
	//             subtracting for decryption
	encrypt = 1
	decrypt = -1

	// buffer size for block file cryption processing
	//bufferSize = 128
)

// byteCrypt encrypts or decrypts a single byte dependent on the mode
func byteCrypt(i, k byte, mode int) byte {

	return byte(int(i) + (mode * int(k)))

}

// SliceCrypt encrypts or decrypts a slice dependent on the mode
func SliceCrypt(i, k []byte, mode int) []byte {

	//initialize the output slice
	var o []byte

	for idx := range i {
		// crypt a single byte and append to the output *os.Fileslice
		o = append(o, byteCrypt(byte(i[idx]), k[idx], mode))
	}
	// return the encrypted/decrypted slice
	return o
}

// FileCrypt encrypts or decrypts the given files according to
// the given parameter pointers passed to it,
// using the passed buffer size and starting the key file after
// the passed pin integer.
// It only returns an error if applicable.
func FileCrypt(inf, keyf, outf *os.File, bufsz *int, pin *int, mode *int) error {

	var err error
	// Setup slice for input
	is := make([]byte, *bufsz)

	// Setup slice for key
	ks := make([]byte, *bufsz)

	// create buffers for reading thefiles
	inbufr := bufio.NewReader(inf)
	keybufr := bufio.NewReader(keyf)

	// loop through the inut file and key file, getting slices to enc/decrypt
	l := 0
	for err != io.EOF {

		// read message file slice

		is, l, err = InFileRead(inbufr, is)
		if err == io.EOF {
			break
		}
		//fmt.Printf("here is the infile read: %v\n", is)

		// read key file slice same length
		ks, err = KeyFileRead(keybufr, ks, l, *pin)
		if err != nil {
			if err != io.EOF {

				log.Fatal(fmt.Errorf("Error reading buffer %v because --> %v", keybufr, err))
			}
		}

		// fmt.Printf("here is the keyfile read: %v\n", ks)

		// encrypt or decrypt the slice dependent on mode
		bytslc := SliceCrypt(is, ks, *mode)

		// append to the output file
		// only write length of slice to avoid padding on the last slice
		// that maybe a short slice if file not a multiple of memory buffer size
		_, err = outf.Write(bytslc[:l])
		if err != nil {
			log.Fatal(fmt.Errorf("Error writing slice %v because --> %v", bytslc, err))
		}
	}
	if err != io.EOF {
		return err
	}
	return nil
}

// InFileRead reads a slice of the input file
func InFileRead(b *bufio.Reader, s []byte) ([]byte, int, error) {

	l, err := b.Read(s)
	if err != nil {
		if err != io.EOF {
			log.Fatal(fmt.Errorf("Error reading buffer %v because --> %v", b, err))
		}
	}

	return s, l, err
}

// Needed to handle the pin read in KeyFileRead on the
// first request for a slice only
var firstTime bool = true

// KeyFileRead reads a slice of the key file dropping the consecutive repeats of
// a same value byte to avoid fortuitous display of parts of message.
// Note that this is what requires a different read for message and key.
func KeyFileRead(b *bufio.Reader, s []byte, l int, pin int) ([]byte, error) {

	// These variables are only needed within an invocation of the function
	// hence they need be declared here only
	var oldByte, newByte byte
	var err error
	// advance by pin number of bytes!
	if firstTime {
		firstTime = false
		for i := 0; i < pin; {
			_, err = b.ReadByte()
			i += 1
			// any error, including EOF, is a failure as key file size
			// should be greater then data file
			if err != nil {
				log.Fatal(fmt.Errorf("Error reading buffer %v for pin because --> %v", b, err))
			}
		}
	}
	for i := 0; i < l; {
		newByte, err = b.ReadByte()
		// any error, including EOF, is a failure as key file size
		// should be greater then data file
		if err != nil {

			log.Fatal(fmt.Errorf("Error reading buffer %v after read for pin because --> %v", b, err))

		}
		if newByte != oldByte {
			s[i] = newByte
			oldByte = newByte
			i++
		}

	}

	return s, err
}
